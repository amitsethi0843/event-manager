import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {Ng2BootstrapModule} from "ng2-bootstrap"
import { AppComponent } from './app.component';

import {routing} from "./app.routing"
import {UserRegistrationComponent} from './components/user/registerUser';
import {LoginComponent} from './components/user/login'
import {HomeComponent} from './components/home'
import {EventListComponent} from './components/event/eventList/eventList';
import {EventDetailComponent} from './components/event/eventDetail/eventDetail';
import {AddEventComponent} from './components/event/addEvent/addEvent';
import {AboutComponent} from './components/about';
import {Auth} from "./services/auth";
import {CustomEventsService} from "./services/customEvents";

import {GoogleMapDirective} from "./components/directives/google/map";
import {Timepicker} from "./components/directives/jqueryUI/timepicker"
import {DatePicker_Self} from "./components/directives/datepicker"

import {ParseDate} from "./pipes/customDate"
import {Capitalize} from "./pipes/capitalize"


@NgModule({
  declarations: [
    AppComponent,
    UserRegistrationComponent,
    LoginComponent,
    HomeComponent,
    EventListComponent,
    EventDetailComponent,
    AddEventComponent,
    AboutComponent,
    GoogleMapDirective,
    Timepicker,
    DatePicker_Self,
    ParseDate,
    Capitalize
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    Ng2BootstrapModule,
    routing
  ],
  providers: [CustomEventsService,
    Auth],
  bootstrap: [AppComponent]
})
export class AppModule { }
