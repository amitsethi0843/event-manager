import {Component} from "@angular/core"
import {CommonService} from "../../services/commonService"
import {CustomEventsService} from "../../services/customEvents"
import {Auth} from "../../services/auth"
import {AppSettings} from "../../config/appSettings"
import { FormGroup,FormControl,FormBuilder,Validators } from '@angular/forms';
import {CustomValidators} from '../../services/validators'

@Component({
  templateUrl: 'registerUser.html',
  providers: [CommonService],
})
export class UserRegistrationComponent {
  registerRequest:any = {
    username: null,
    password: null,
    retypePassword: null
  }
  registrationForm:FormGroup;
  token:any;
  returnedUsername:any;
  showError:boolean;

  constructor(private commonService:CommonService, private auth:Auth, private _fb:FormBuilder,private customEventsService:CustomEventsService) {
    this.customEventsService.changeSidePanelVisibility(true);
    this.registrationForm=this._fb.group({
      username:['', [Validators.required,CustomValidators.validateEmail]],
      password:['',[Validators.required,Validators.minLength(AppSettings.fetch().appServer.passwordLength)]],
      retypePassword:['',[Validators.required,Validators.minLength(AppSettings.fetch().appServer.passwordLength)]]
    })
  }

  registerUser() {
    if(this.registrationForm.valid){
      this.commonService.setData(
        this.registrationForm.value
      );
      this.commonService.setUrl('user/register/');
      this.commonService.postData().subscribe(
        data=> {
          if (data.token && data.username) {
            this.token = data.token;
            this.returnedUsername = data.username;
            this.auth.setUserData(this.token, this.returnedUsername);
          }
        },
        error=>console.log(JSON.stringify(error))
      )
    }
    else{
      this.showError=true;
    }
  }

}
