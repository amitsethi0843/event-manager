import {Component} from "@angular/core"
import {CommonService} from "../../../services/commonService"
import {AppConstants} from "../../../config/constants"
import {CustomEventsService} from "../../../services/customEvents";
import {ActivatedRoute} from "@angular/router"
import {OnInit} from "@angular/core"

@Component({
  templateUrl: 'eventDetail.html',
  providers: [CommonService]
})
export class EventDetailComponent implements OnInit {
  eventId:string;
  eventDetails:any;
  stringifiedData:any;
  nearByRailywayStations:Array<any> = [];
  nearByMetroStations:Array<any> = [];
  nearByHotels:Array<any> = [];
  latitude:string;
  sub:any;
  longitude:string;
  error:any;

  constructor(private commonService:CommonService, private customEventsService:CustomEventsService, private route:ActivatedRoute,) {

  }

  ngOnInit() {
    this.customEventsService.changeSidePanelVisibility(true);
    this.sub = this.route
      .params
      .subscribe(params=> {
        this.eventId = params['id'];
      })
    this.fetchEventById()
  }

  fetchEventById() {
    if (this.eventId) {
      this.commonService.setUrl("event/" + this.eventId + "/");
      this.commonService.getData().subscribe(
        data=> {
          var nearByLocations = data.nearby_locations;
          if (nearByLocations) {
            for (var i in nearByLocations) {
              if (nearByLocations[i].location.type === AppConstants._fetch().locationType.METROSTATION[0]) {
                this.nearByMetroStations.push(nearByLocations[i])
              }
              else if (nearByLocations[i].location.type === AppConstants._fetch().locationType.RAILWAYSTATION[0]) {
                this.nearByRailywayStations.push(nearByLocations[i])
              }
              else if (nearByLocations[i].location.type === AppConstants._fetch().locationType.HOTEL[0]) {
                this.nearByHotels.push(nearByLocations[i])
              }
            }
          }
          this.eventDetails = data;
          this.latitude = data.location.latitude;
          this.longitude = data.location.longitude;
        },
        error=> {
          this.error = error
        }
      )
    }
  }
}
