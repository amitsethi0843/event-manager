import {Component} from '@angular/core'
import {Input} from "@angular/core";
import {CommonService} from "../../../services/commonService";
//import {OnInit} from "angular2/core";
import {OnInit} from "@angular/core";
import {CustomEventsService} from "../../../services/customEvents";

@Component({
  providers: [CommonService],
  templateUrl: 'eventList.html'
})
export class EventListComponent implements OnInit {

  events:any;

  eventTypes:any = {
    values: [
      {key: "SINGLE", value: "Single"},
      {key: "COUPLE", value: "Couple"},
      {key: "FAMILY", value: "Family"}
    ]
  };

  eventFilter:any = {
    fromDate: null,
    toDate: null
  };

  constructor(private commonService:CommonService,private customEventsService:CustomEventsService) {
    this.customEventsService.changeSidePanelVisibility(true);
  }

  ngOnInit() {
    this.fetchEventList();
  }

  fetchEventList() {
    this.commonService.setUrl("event/");
    this.commonService.getData().subscribe(
      data=> {
        this.events = data.results;
      },
      error=>console.log(JSON.stringify(error))
    );

  }
}
