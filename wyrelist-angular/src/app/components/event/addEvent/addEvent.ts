import {Component} from "@angular/core"
import {CommonService} from "../../../services/commonService"
import {NgModel} from "@angular/forms"
import {CustomEventsService} from "../../../services/customEvents";
@Component({
  templateUrl: 'addEvent.html',
  providers: [CommonService,NgModel],
})

export class AddEventComponent {
  formElementsValues:any = {
    eventType: [
      {key: "SINGLE", value: "Single"},
      {key: "COUPLE", value: "Couple"},
      {key: "FAMILY", value: "Family"}
    ]
  }
  input:any = {
    contact: {
      contactNumber: null,
      contactType: null,
    },
    address: {
      line1: null,
      line2: null,
      area: null,
      city: null,
      state: null,
      pincode: null,
      latitude: null,
      longitude: null
    },
    entry: {
      type: null,
      entryChargable: false,
      fees: 0,
    },
    event: {
      date: null,
      duration: 1,
      eventStartTime: null,
      eventEndTime: null,
      description: null,
      totalTickets: null
    },
    createdBy: null
  };

  constructor(private commonService:CommonService,private customEventsService:CustomEventsService) {
    this.customEventsService.changeSidePanelVisibility(true);

  }

  addEvent() {
    console.log(JSON.stringify(this.input.event))
  }

}
