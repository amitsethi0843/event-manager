import { Component } from '@angular/core';
import { Router } from '@angular/router';

import {Auth} from "./services/auth";
import {CustomEventsService} from "./services/customEvents";
import {OnInit} from "@angular/core";

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: "../app/templates/routeLayout.html"
  //styleUrls: ['app.component.css']
})

export class AppComponent implements OnInit {
  logedInUserName:string;
  userToken:string;
  showSidePanel:boolean=true;
  title:string="Wyrelist";

  constructor(private router:Router,
              private auth:Auth,private customEventsService:CustomEventsService) {
    this.auth.dataChange.subscribe(data=> {
      this.userToken = data[0];
      this.logedInUserName = data[1];
      if (this.userToken) {
        this.router.navigate(['EventList'])
      }
      else {
        this.router.navigate(['Home'])
      }
    });

    this.customEventsService.sidePanelVisibilityChange.subscribe(data=>{
      this.showSidePanel=data
    })
    this.userToken = this.auth.getUserToken();
    this.logedInUserName = this.auth.getUserName();

    if (this.userToken) {
      this.router.navigate(['EventList'])
    }
    else {
      this.router.navigate(['Home'])
    }
  }

  ngOnInit() {

  }
  enableSidePanel(){
    this.showSidePanel=true
  }
  disableSidePabel(){
    this.showSidePanel=false
  }
  logout() {
    this.auth.removeUserData();
  }

}
