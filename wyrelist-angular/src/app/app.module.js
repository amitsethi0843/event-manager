var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var platform_browser_1 = require('@angular/platform-browser');
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var ng2_bootstrap_1 = require("ng2-bootstrap");
var app_component_1 = require('./app.component');
var app_routing_1 = require("./app.routing");
var registerUser_1 = require('./components/user/registerUser');
var login_1 = require('./components/user/login');
var home_1 = require('./components/home');
var eventList_1 = require('./components/event/eventList/eventList');
var eventDetail_1 = require('./components/event/eventDetail/eventDetail');
var addEvent_1 = require('./components/event/addEvent/addEvent');
var about_1 = require('./components/about');
var auth_1 = require("./services/auth");
var customEvents_1 = require("./services/customEvents");
var map_1 = require("./components/directives/google/map");
var timepicker_1 = require("./components/directives/jqueryUI/timepicker");
var datepicker_1 = require("./components/directives/datepicker");
var customDate_1 = require("./pipes/customDate");
var capitalize_1 = require("./pipes/capitalize");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                registerUser_1.UserRegistrationComponent,
                login_1.LoginComponent,
                home_1.HomeComponent,
                eventList_1.EventListComponent,
                eventDetail_1.EventDetailComponent,
                addEvent_1.AddEventComponent,
                about_1.AboutComponent,
                map_1.GoogleMapDirective,
                timepicker_1.Timepicker,
                datepicker_1.DatePicker_Self,
                customDate_1.ParseDate,
                capitalize_1.Capitalize
            ],
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                http_1.HttpModule,
                ng2_bootstrap_1.Ng2BootstrapModule,
                app_routing_1.routing
            ],
            providers: [customEvents_1.CustomEventsService,
                auth_1.Auth],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
})();
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map