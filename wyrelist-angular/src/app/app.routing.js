var router_1 = require("@angular/router");
var registerUser_1 = require('./components/user/registerUser');
var login_1 = require('./components/user/login');
var home_1 = require('./components/home');
var eventList_1 = require('./components/event/eventList/eventList');
var eventDetail_1 = require('./components/event/eventDetail/eventDetail');
var addEvent_1 = require('./components/event/addEvent/addEvent');
var about_1 = require('./components/about');
var appRoutes = [
    { path: '', component: home_1.HomeComponent },
    { path: 'login', component: login_1.LoginComponent },
    { path: 'register', component: registerUser_1.UserRegistrationComponent },
    { path: 'events', component: eventList_1.EventListComponent },
    { path: 'event/:id', component: eventDetail_1.EventDetailComponent },
    { path: 'addEvent', component: addEvent_1.AddEventComponent },
    { path: 'about', component: about_1.AboutComponent },
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map