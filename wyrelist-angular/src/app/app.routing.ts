import {ModuleWithProviders} from "@angular/core"
import {Routes,RouterModule} from "@angular/router"
import {UserRegistrationComponent} from './components/user/registerUser';
import {LoginComponent} from './components/user/login'
import {HomeComponent} from './components/home'
import {EventListComponent} from './components/event/eventList/eventList';
import {EventDetailComponent} from './components/event/eventDetail/eventDetail';
import {AddEventComponent} from './components/event/addEvent/addEvent';
import {AboutComponent} from './components/about';

const appRoutes:Routes=[
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: UserRegistrationComponent},
  {path: 'events', component: EventListComponent},
  {path: 'event/:id', component: EventDetailComponent},
  {path: 'addEvent', component: AddEventComponent},
  {path: 'about', component: AboutComponent},
];

export const routing:ModuleWithProviders=RouterModule.forRoot(appRoutes)
