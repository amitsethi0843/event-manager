var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var AppComponent = (function () {
    function AppComponent(router, auth, customEventsService) {
        var _this = this;
        this.router = router;
        this.auth = auth;
        this.customEventsService = customEventsService;
        this.showSidePanel = true;
        this.title = "Wyrelist";
        this.auth.dataChange.subscribe(function (data) {
            _this.userToken = data[0];
            _this.logedInUserName = data[1];
            if (_this.userToken) {
                _this.router.navigate(['/events']);
            }
            else {
                _this.router.navigate(['/']);
            }
        });
        this.customEventsService.sidePanelVisibilityChange.subscribe(function (data) {
            _this.showSidePanel = data;
        });
        this.userToken = this.auth.getUserToken();
        this.logedInUserName = this.auth.getUserName();
        if (this.userToken) {
            this.router.navigate(['/events']);
        }
        else {
            this.router.navigate(['/']);
        }
    }
    AppComponent.prototype.enableSidePanel = function () {
        this.showSidePanel = true;
    };
    AppComponent.prototype.disableSidePabel = function () {
        this.showSidePanel = false;
    };
    AppComponent.prototype.logout = function () {
        this.auth.removeUserData();
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: "../app/templates/routeLayout.html"
        })
    ], AppComponent);
    return AppComponent;
})();
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map